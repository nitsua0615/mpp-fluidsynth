# mpp-fluidsynth
A fork of multiplayerpiano that uses fluidsynth with a SoundFont and allows selection of patches.
[https://mpp.zipdox.net/](https://mpp.zipdox.net/)

## Features
- Lower input lag than MPP (20ms reduction)
- Better sound
- WASM accelerated
- Many presets
- SoundFonts cached to skip loading on subsequent visits
