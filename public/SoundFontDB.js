class SoundFontDB{
    /**
     * Initialize the database
     */
    init(){
        const self = this;
        return new Promise((resolve, reject)=>{
            const dbRequest = window.indexedDB.open('SoundFonts');
            dbRequest.onerror = reject;
            dbRequest.onupgradeneeded = dbRequest.onversionchange = function(event){
                self.db = event.target.result;
                if(!self.db.objectStoreNames.contains('soundfonts')){
                    const objectStore = self.db.createObjectStore('soundfonts');
                    objectStore.createIndex('soundfont', 'name');
                }
            }
            dbRequest.onsuccess = function(event){
                self.db = event.target.result;
                resolve();
            }
        });
    }

    /**
     * Load a soundfont
     * @param  {String}      name Name of the soundfont
     * @param  {ArrayBuffer} file The soundfont file
     */
    storeSoundFont(name, file){
        const self = this;
        return new Promise((resolve, reject)=>{
            const transaction = self.db.transaction(['soundfonts'], 'readwrite');
            const objectStore = transaction.objectStore('soundfonts');
            const request = objectStore.add(file, name);
            request.onsuccess = resolve;
            request.onerror = reject;
        });
    }
    
    /**
     * Retrieve a soundfont
     * @param  {String} name Name of the soundfont
     */
    getSoundFont(name){
        const self = this;
        return new Promise((resolve, reject)=>{
            const transaction = self.db.transaction(['soundfonts'], 'readwrite');
            const objectStore = transaction.objectStore('soundfonts');
            const request = objectStore.get(name);
            request.onsuccess = function(event){
                resolve(event.target.result);
            }
            request.onerror = reject;
        });
    }
}